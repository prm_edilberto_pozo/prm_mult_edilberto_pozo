package optfn.com.videos;

import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    VideoView videoView;

    MediaController mediaController;

    @Override public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        videoView =(VideoView)findViewById(R.id.video_view);

        mediaController = new MediaController(this);

        mediaController.setAnchorView(videoView);


        videoView.setMediaController(mediaController);

        videoView.setVideoPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString() + "/video.mp4");
        //videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
        //videoView.setVideoURI(Uri.parse("http://edilbertopozo.esy.es/video.mp4"));


        videoView.start();

        videoView.requestFocus();

    }
    @Override
    public boolean onTouchEvent (MotionEvent event) {

        mediaController.show();

        return false;

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(videoView!=null){

            outState.putInt("posi",videoView.getCurrentPosition());
            //outState.putInt("stat",status);
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null && videoView!=null){

            videoView.seekTo(savedInstanceState.getInt("posi"));
            //status = savedInstanceState.getInt("stat");
        }

    }
}
