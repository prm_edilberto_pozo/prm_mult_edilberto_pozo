package com.example.usuario.serviciomusica;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by Usuario on 14/01/2017.
 */

public class ServicioMusica extends IntentService {
    MediaPlayer reproductor;
    public ServicioMusica() {

        super ("ServicioMusica");
//        try {
//            reproductor = new MediaPlayer();
//            reproductor.setDataSource("android.resource://"+ "com.example.usuario.serviciomusica" + "/" + R.raw.audio);
//            reproductor.prepare();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    @Override
    protected void onHandleIntent(Intent intencion) {

    }
    @Override
    public void onCreate() {
        Toast.makeText(this,"Servicio creado",
                Toast.LENGTH_SHORT).show();
        reproductor = MediaPlayer.create(this, R.raw.audio);
    }

    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
        Toast.makeText(this,"Servicio arrancado "+ idArranque,
                Toast.LENGTH_SHORT).show();
        reproductor.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this,"Servicio detenido",
                Toast.LENGTH_SHORT).show();
        reproductor.stop();
    }

    @Override
    public IBinder onBind(Intent intencion) {
        return null;
    }
}