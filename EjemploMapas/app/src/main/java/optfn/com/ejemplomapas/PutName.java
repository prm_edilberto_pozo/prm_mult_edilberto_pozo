package optfn.com.ejemplomapas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * Created by b13-06m on 24/01/2017.
 */

public class PutName extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
    }
    public void send(View view){
        EditText editText = (EditText)findViewById(R.id.editText);

        Intent intent = this.getIntent();
        intent.putExtra("titulo", editText.getText());
        this.setResult(RESULT_OK, intent);
        finish();
    }
}
