package es.javier.mapas;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    CameraUpdate cam;
    private LatLng punto;
    private LatLng puntoant;
    PolylineOptions linea;

    EditText la;
    EditText lo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        la=(EditText)findViewById(R.id.etLatitud);
        lo=(EditText)findViewById(R.id.etLongitud);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        linea= new PolylineOptions();



        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(LatLng point) {
                double lat=point.latitude;
                double lng=point.longitude;
                String tituloMarcador="Mi marcador "+String.valueOf(lat)+", "+String.valueOf(lng);
                mMap.addMarker(
                        new MarkerOptions().position(new LatLng(point.latitude, point.longitude))
                                .title(tituloMarcador));
                //puntoant=new LatLng(punto.latitude, punto.longitude);
                //punto=new LatLng(point.latitude, point.longitude);



                linea.add(new LatLng(point.latitude, point.longitude));
                linea.width(3);
                linea.color(Color.GREEN);
                mMap.addPolyline(linea);



            }
        });
    }


    public void pamadrid(View view){
        LatLng Madrid = new LatLng(40.417325, -3.683081);
        //CameraUpdate camaraOpciones = CameraUpdateFactory.newLatLng(Madrid);
        //CameraUpdateFactory.newLatLng(Madrid);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(Madrid));

    }

    public void padentro(View view){
        //LatLng Madrid = new LatLng(40.417325, -3.683081);
        //CameraUpdateFactory.newLatLngZoom(Madrid, 5F);
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Madrid, 5F));
        cam= CameraUpdateFactory.zoomIn();

        mMap.animateCamera(cam);
    }

    public void pafuera(View view){
        //LatLng Madrid = new LatLng(40.417325, -3.683081);
        //CameraUpdateFactory.newLatLngZoom(Madrid, 5F);
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Madrid, 2F));
        cam= CameraUpdateFactory.zoomOut();

        mMap.animateCamera(cam);
    }

    public void addMarker(View view){
        double lat=Double.parseDouble(la.getText().toString());
        double lng=Double.parseDouble(lo.getText().toString());
        String tituloMarcador="Mi marcador "+String.valueOf(lat)+", "+String.valueOf(lng);
        mMap.addMarker(
                new MarkerOptions().position(new LatLng(lat, lng))
                        .title(tituloMarcador));
        //puntoant=new LatLng(punto.latitude, punto.longitude);
        //punto=new LatLng(point.latitude, point.longitude);



        linea.add(new LatLng(lat, lng));
        linea.width(3);
        linea.color(Color.GREEN);
        mMap.addPolyline(linea);

    }

    public void ir(View view){
        LatLng pos = new LatLng(Double.parseDouble(la.getText().toString()), Double.parseDouble(lo.getText().toString()));
        //CameraUpdate camaraOpciones = CameraUpdateFactory.newLatLng(Madrid);
        //CameraUpdateFactory.newLatLng(Madrid);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(pos));

    }

}
