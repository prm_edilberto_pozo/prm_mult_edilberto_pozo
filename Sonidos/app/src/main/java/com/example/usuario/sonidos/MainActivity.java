package com.example.usuario.sonidos;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;

import static android.R.attr.path;
import static android.os.Environment.getExternalStorageDirectory;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private int status;
    private MediaPlayer mediaPlayerB;
    private int statusB;
    private SoundPool soundPool;
    private int idSonido1;
    private int idSonido2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mediaPlayer = MediaPlayer.create(this,R.raw.adrenalina);
        status = 2;

        soundPool = new SoundPool( 5, AudioManager.STREAM_MUSIC , 0);

        idSonido1 = soundPool.load(this, R.raw.sonido1, 0);
        idSonido2 = soundPool.load(this, R.raw.sonido2, 0);

        mediaPlayerB = new MediaPlayer();
        try {
            mediaPlayerB.setDataSource(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString() + "/song.mp3");
            mediaPlayerB.prepare();

        } catch (IOException e) {
            e.printStackTrace();
        }


        statusB = 2;
    }


    public void pool1(View view){
        soundPool.play(idSonido1, 1, 1, 1, 0, 1);

    }
    public void pool2(View view){
        soundPool.play(idSonido2, 1, 1, 1, 0, 1);

    }
    public void play(View view){
        if (status == 2){
            mediaPlayer.start();
            status = 1;
        }
        if (status == 3){
            mediaPlayer = MediaPlayer.create(this,R.raw.adrenalina);
            mediaPlayer.start();
            status = 1;

        }

    }
    public void pause(View view){
        if(status == 1){
            mediaPlayer.pause();
            status = 2;
        }

    }
    public void stop(View view){
        if(status == 1){
            mediaPlayer.stop();
            status = 3;
        }

    }
    public void playB(View view){
        if (statusB == 2){
            mediaPlayerB.start();
            statusB = 1;
        }
        if (statusB == 3){
            mediaPlayerB = new MediaPlayer();
            try {
                mediaPlayerB.setDataSource(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString() + "/song.mp3");
                mediaPlayerB.prepare();

            } catch (IOException e) {
                e.printStackTrace();
            }

            mediaPlayerB.start();
            statusB = 1;

        }

    }
    public void pauseB(View view){
        if(statusB == 1){
            mediaPlayerB.pause();
            statusB = 2;
        }

    }
    public void stopB(View view){
        if(statusB == 1){
            mediaPlayerB.stop();
            statusB = 3;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mediaPlayerB!=null){

            outState.putInt("posi",mediaPlayerB.getCurrentPosition());
            outState.putInt("stat",statusB);
            mediaPlayerB.pause();
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null){

            mediaPlayerB.seekTo(savedInstanceState.getInt("posi"));
            mediaPlayerB.start();
            statusB = savedInstanceState.getInt("stat");
        }

    }
}
